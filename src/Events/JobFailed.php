<?php
namespace DPS\AwsWorker\Events;

use DPS\AwsWorker\Jobs\AwsJob;
use Exception;

class JobFailed
{
    /**
     * @var AwsJob
     */
    public $job;
    /**
     * @var Exception
     */
    private $exception;

    /**
     * JobRan constructor.
     * @param AwsJob $job
     * @param Exception $exception
     */
    public function __construct(AwsJob $job, Exception $exception)
    {
        $this->job = $job;
        $this->exception = $exception;
    }
}