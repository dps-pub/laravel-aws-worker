<?php
namespace DPS\AwsWorker\Events;

use DPS\AwsWorker\Jobs\AwsJob;

class JobRan
{
    /**
     * @var AwsJob
     */
    public $job;

    /**
     * JobRan constructor.
     * @param AwsJob $job
     */
    public function __construct(AwsJob $job)
    {
        $this->job = $job;
    }
}