<?php
namespace DPS\AwsWorker;

use Illuminate\Console\Scheduling\Event;

class EventRan
{
    /**
     * @var Event
     */
    public $event;

    /**
     * EventRan constructor.
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }
}