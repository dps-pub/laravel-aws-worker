<?php

namespace DPS\AwsWorker\Wrappers;

/**
 * Interface WorkerInterface
 * @package DPS\AwsWorker\Wrappers
 */
interface WorkerInterface
{
    /**
     * @param $queue
     * @param $job
     * @param array $options
     * @return mixed
     */
    public function process($queue, $job, array $options);
}
