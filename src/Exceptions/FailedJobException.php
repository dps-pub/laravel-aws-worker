<?php

namespace DPS\AwsWorker\Exceptions;

/**
 * Class FailedJobException
 * @package DPS\AwsWorker\Exceptions
 */
class FailedJobException extends \Exception
{
}
