<?php

namespace DPS\AwsWorker\Exceptions;

/**
 * Class MalformedRequestException
 * @package DPS\AwsWorker\Exceptions
 */
class MalformedRequestException extends \Exception
{
}
